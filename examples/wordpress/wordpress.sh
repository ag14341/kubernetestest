
cat <<EOF >./kustomization.yaml
secretGenerator:
- name: mysql-pass
  literals:
  - password=password
resources:
  - mysql-deployment.yaml
  - wordpress-deployment.yaml
EOF
kubectl apply -k ./
kubectl get services
