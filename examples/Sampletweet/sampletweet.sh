# make sure you are inside the kubernetes-tutorial directory
#https://auth0.com/blog/amp/kubernetes-tutorial-step-by-step-introduction-to-basic-concepts/
kubectl apply -f deployment.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/mandatory.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/cloud-generic.yaml
kubectl apply -f service.yaml
kubectl apply -f ingress.yaml
kubectl get svc \
  -n ingress-nginx ingress-nginx \
  -o=jsonpath='{.status.loadBalancer.ingress[0].ip}'
