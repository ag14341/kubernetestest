### done on Gardener sap kubernetes shoot clusters
###https://dashboard.garden.canary.k8s.ondemand.com/namespace/garden-manjutest/shoots/nqtdqfzkr5/

###https://www.fairwinds.com/blog/how-to-deploy-multi-tiered-web-application-with-kubernetes
- http://af6bd5df833d14ddca5cecb63c7d4352-594173792.eu-central-1.elb.amazonaws.com/
- image you can find at https://gitlab.com/ag14341/kubernetestest/-/blob/master/k8working1.png

###https://github.com/kubernetes/examples/tree/master/guestbook-go
- http://a5ec06c6ee1f94ecbac39c05db2be3fc-1095681576.eu-central-1.elb.amazonaws.com:3000/
- image you can find at https://gitlab.com/ag14341/kubernetestest/-/blob/master/k8working2.png

### https://kubernetes.io/docs/tutorials/stateful-application/mysql-wordpress-persistent-volume/
- http://aff3c0ab6fd984f298c9684874083113-1118098251.eu-central-1.elb.amazonaws.com/wp-admin/update-core.php
- image you can find at https://gitlab.com/ag14341/kubernetestest/-/blob/master/k8working3.png

## Yet to be tested
- https://www.fairwinds.com/blog/how-to-deploy-multi-tiered-web-application-with-kubernetes
- https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/
- https://blog.mayadata.io/openebs/steps-to-deploy-angular-application-on-kubernetes
- https://collabnix.github.io/kubelabs/pods101/deploy-your-first-nginx-pod.html
- https://www.freecodecamp.org/news/learn-kubernetes-in-under-3-hours-a-detailed-guide-to-orchestrating-containers-114ff420e882/
- https://blog.postman.com/kubernetes-tutorial/
- https://timber.io/blog/collecting-application-logs-on-kubernetes/


